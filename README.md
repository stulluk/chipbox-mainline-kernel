# chipbox-mainline-kernel

This is the mainline kernel for chipbox

Use this toolchain for convenience: gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabi.tar.xz

Please run ```make -f chipbox.mak config``` and then ```make -f chipbox.mak build```

