CROSS_COMPILE	?= arm-linux-gnueabi-
build_dir       := $(CURDIR)/build-chipbox
output_dir	:= $(HOME)
rootfs		:= $(HOME)/rootfs-chipbox.cpio
install_dir     := $(build_dir)/install
config_file     := $(build_dir)/.config
strip		:= $(CROSS_COMPILE)strip
objcopy		:= $(CROSS_COMPILE)objcopy
rootfsbase	:= $(shell basename $(rootfs))
#dtb		:= $(build_dir)/arch/arm/boot/dts/versatile-ab.dtb
dtb		:= $(build_dir)/arch/arm/boot/dts/chipbox.dtb
makejobs	:= $(shell grep '^processor' /proc/cpuinfo | sort -u | wc -l)
makethreads	:= $(shell dc -e "$(makejobs) 1 + p")
tftpbootdir := "/media/DATA/MERIH-YEDEK/TFTPROOT/"

make_options := -f Makefile \
		-j$(makethreads) -l$(makejobs) \
                ARCH=arm \
                CROSS_COMPILE=$(CROSS_COMPILE) \
                KBUILD_OUTPUT=$(build_dir)

.PHONY: help
help:
	@echo "****  Common Makefile  ****"
	@echo "example:"
	@echo "make -f chipbox.mak config"
	@echo "make -f chipbox.mak build"

have-rootfs: FORCE
	@if [ ! -f $(rootfs) ] ; then \
	     echo "ERROR: no rootfs at $(rootfs)" ; \
	     echo "This is needed to boot the system." ; \
	     echo "ABORTING." ; \
	     exit 1 ; \
	else \
	     echo "Rootfs available at $(rootfs)" ; \
	fi

have-crosscompiler: FORCE
	@echo -n "Check that $(CROSS_COMPILE)gcc is available..."
	@which $(CROSS_COMPILE)gcc > /dev/null ; \
	if [ ! $$? -eq 0 ] ; then \
	   echo "ERROR: cross-compiler $(CROSS_COMPILE)gcc not in PATH=$$PATH!" ; \
	   echo "ABORTING." ; \
	   exit 1 ; \
	else \
	   echo "OK" ;\
	fi

config-base: FORCE
	@mkdir -p $(build_dir)
	@if [ -r $(config_file) ] ; then \
		rm $(config_file) ; \
	fi
	$(MAKE) $(make_options) chipbox_defconfig

config-initramfs: config-base have-rootfs
	@cp $(rootfs) $(build_dir)/$(rootfsbase)
	# Configure in the initramfs
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable BLK_DEV_INITRD \
	--set-str INITRAMFS_SOURCE $(rootfsbase) \
	--enable RD_GZIP \
	--enable INITRAMFS_COMPRESSION_GZIP

config-kasan: config-base
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable SLUB \
	--enable SLUB_DEBUG_ON \
	--enable KASAN \
	--enable KASAN_OUTLINE \
	--enable STACKTRACE \
	--enable TEST_KASAN

config-mainlined-features: config-base
	# Then reconfigure various stuff
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable DEBUG_FS \
	--enable NEW_LEDS \
	--enable LEDS_CLASS \
	--enable LEDS_TRIGGERS \
	--enable LEDS_TRIGGER_HEARTBEAT \
	--enable NO_HZ_COMMON \
	--enable NO_HZ_IDLE \
	--enable HIGH_RES_TIMERS \
	--enable MMC_ARMMMCI \
	--enable SPI \
	--enable I2C \
	--enable I2C_VERSATILE \
	--enable RTC_LIB \
	--enable RTC_CLASS \
	--enable RTC_DRV_DS1307 \
	--enable MFD_SYSCON \
	--enable LEDS_SYSCON \
	--disable LEDS_VERSATILE \
	--enable MTD_PHYSMAP_OF \
	--enable MTD_CFI \
	--enable MTD_CFI_INTELEXT \
	--enable MTD_CFI_AMDSTD \
	--enable IOSCHED_BFQ \
	--enable BFQ_GROUP_IOSCHED \
	--enable CMA \
	--enable DRM \
	--enable DRM_PL111 \
	--enable FRAMEBUFFER_CONSOLE \
	--enable DRM_PANEL \
	--enable DRM_PANEL_SIMPLE \
	--enable DRM_BRIDGE \
	--enable DRM_PANEL_BRIDGE \
	--enable DRM_SIMPLE_BRIDGE \
	--enable DRM_DISPLAY_CONNECTOR \
	--enable DRM_PANEL_ARM_VERSATILE \
	--enable LOGO

# For early printk
config-earlydebug: config-base
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable DEBUG_LL_UART_8250 \
	--enable EARLY_PRINTK \
	--set-str CMDLINE "earlyprintk"

config-devicetree: config-base
	# Configure in the optional device tree if available
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable USE_OF \
	--enable MACH_VERSATILE_DT \
	--disable ARCH_VERSATILE_PB \
	--disable MACH_VERSATILE_AB \
	--enable ARM_APPENDED_DTB \
	--enable ARM_ATAG_DTB_COMPAT \
	--enable ATAGS \
	--enable PROC_DEVICETREE

config-multiplatform: config-base
	$(CURDIR)/scripts/config --file $(config_file) \
	--disable ARCH_MULTI_V7 \
	--enable ARCH_MULTI_V5 \
	--enable ARCH_NOMADIK

config-ftrace: config-base
	$(CURDIR)/scripts/config --file $(config_file) \
	--enable FTRACE \
	--enable FUNCTION_TRACER

config: have-rootfs config-base config-initramfs config-mainlined-features config-earlydebug config-devicetree config-kasan FORCE
	yes "" | make $(make_options) oldconfig

menuconfig: FORCE
	$(MAKE) $(make_options) menuconfig

saveconfig: config-base config-mainlined-features
	yes "" | make $(make_options) oldconfig
	$(MAKE) $(make_options) savedefconfig
	cp $(build_dir)/defconfig arch/arm/configs/chipbox_defconfig

build-zimage: have-crosscompiler
	$(MAKE) $(make_options) -j 4 zImage CONFIG_DEBUG_SECTION_MISMATCH=y

build-dtbs: FORCE
	$(MAKE) $(make_options) -j 4 dtbs

check-bindings: FORCE
	$(MAKE) $(make_options) dt_binding_check -k $(make_flags)
	$(MAKE) $(make_options) dtbs_check -k $(make_flags)

build: have-rootfs have-crosscompiler build-zimage build-dtbs
	@echo "Copy vmlinux to $(output_dir)/vmlinux..."
	@cp -f $(build_dir)/vmlinux $(output_dir)/vmlinux
	@echo "Copy zImage to $(output_dir)/zImage..."
	@if [ -r $(build_dir)/arch/arm/boot/zImage ] ; then \
	    cp $(build_dir)/arch/arm/boot/zImage $(output_dir)/zImage ; \
	fi
	@if [ -r $(dtb) ] ; then \
	   echo "Catenate DTB onto zImage ..." ; \
	   cat $(dtb) >> $(output_dir)/zImage ; \
	else \
		echo "Error!! DTB not found!!"; \
		exit 1; \
	fi
	# Generate uImage
	mkimage \
		-A arm \
		-O linux \
		-T kernel \
		-C none \
		-a 0x00008000 \
		-e 0x00008000 \
		-n "Chipbox Linux kernel (DT)" \
		-d $(output_dir)/zImage \
		$(output_dir)/uImage
	# If we have a TFTP boot directory
	@if [ -w $(tftpbootdir) ] ; then \
	  echo "copy uImage to $(tftpbootdir)" ; \
	  cp $(output_dir)/uImage $(tftpbootdir) ; \
	fi
	@echo "Boot from ethernet:"
	@echo "setenv serverip 192.168.1.121;setenv ipaddr 192.168.1.134;setenv bootfile uImage;tftpboot;bootm"

TAGS: FORCE
	$(MAKE) $(make_options) TAGS

clean:
	$(MAKE) -f Makefile clean
	rm -f $(module_files)
	rm -rf build

# Rules without commands or prerequisites that do not match a file name
# are considered to always change when make runs. This means that any rule
# that depends on FORCE will always be remade also.
FORCE:
